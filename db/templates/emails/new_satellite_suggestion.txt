{% extends 'emails/base.txt' %}

{% block main %}
New satellite suggestion for satellite {{ data.satname }} with Satellite Identifier {{ data.satid }} was submitted by user {{ data.contributor }}!

Current satellite suggestions awaiting approval for {{ data.satname }}: {{ data.suggestion_count }}

Review and approve the suggestions at the URL below

{{ data.saturl }}

{% endblock %}
